/*
 * @Description: 配置文件
 * @Author: Vergil
 * @Date: 2021-08-23 22:40:53
 * @LastEditTime: 2021-08-31 14:22:41
 * @LastEditors: Vergil
 */
exports.data = [
  {
      folder:'button',
      componentsName:'button',
  },
  {
      folder:'radio',
      componentsName:'radio',
  }
];