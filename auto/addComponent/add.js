const fs = require("fs");
const path = require("path");
//引入配置文件
const configData = require("./data");
//引入文件模板
let componentsPage = require("../page/index");
// index模板
let indexPage = componentsPage.indexPage;
//vue模板
let vuePage = componentsPage.vuePage;
//style模板
let stylePage = componentsPage.stylePage;
// 创建目录
function mkdirsSync(dirname) {
    //判断目录是否存在
  if (fs.existsSync(dirname)) {
      return true;
  } else {
      if (mkdirsSync(path.dirname(dirname))) {
          fs.mkdirSync(dirname);
          return console.log(`创建目录成功-${dirname}`);
      }
  }   
}
mkdirsSync(`./src/packages`);
mkdirsSync(`./src/styles`);
//遍历配置文件并调用创建目录方法
configData.data.forEach((item) => {
    console.log(item.folder);
  if(item.folder){
      mkdirsSync(`./src/packages/${item.folder}`);
  }
});
// console.log('>>>>>>>>>>>>>');
// console.log('开始创建文件');
//遍历配置文件创建index、vue、style文件
configData.data.forEach((item) => {
    if(item.componentsName){
        let indexPath=`./src/packages/${item.folder}/index.js`;
        let vuePath=`./src/packages/${item.folder}/${item.componentsName}.vue`;
        let stylePath=`./src/styles/${item.componentsName}.scss`;
        let name=item.componentsName;
        //创建index.js文件
        if (fs.existsSync(indexPath)) {
            console.log(indexPath+'已存在');
        }else{
            fs.writeFile(indexPath, indexPage(name), function(err){
                if(err){
                    return console.log('创建index文件失败', err);
                }
                console.log(`创建index文件成功！-${name}.js`);
            });
        }
        //创建组件vue文件
        if (fs.existsSync(vuePath)) {
            console.log(vuePath+'已存在');
        }else{
            fs.writeFile(vuePath, vuePage(name), function(err){
                if(err){
                    return console.log('创建vue文件失败', err);
                }
                console.log(`创建vue文件成功！-${name}.vue`);
            });
        }
        //创建组件style文件
        if (fs.existsSync(stylePath)) {
            console.log(stylePath+'已存在');
        }else{
            fs.writeFile(stylePath, stylePage(), function(err){
                if(err){
                    return console.log('创建style文件失败', err);
                }
                console.log(`创建style文件成功！-${name}.scss`);
            });
        }
    }
});
//遍历配置文件获取对应组件拼装组件初始化
function addComponentsList(){
    let file='';
    let allComponents='';
    configData.data.forEach((item) => {
        //遍历所有组件每个组件添加依赖 例如：import Button from './packages/button';
        let name=item.componentsName.charAt(0).toUpperCase() + item.componentsName.slice(1); //首字母大写
        file+=`import ${name} from './packages/${item.componentsName}';\n`;
    });
    configData.data.forEach((item) => {
        //遍历所有组件每个组件添加   例如：lButton: Button,
        let name=item.componentsName.charAt(0).toUpperCase() + item.componentsName.slice(1); //首字母大写
        allComponents+=`l${name}: ${name},\n`;
    });
    file +=`const allComponents ={\n${allComponents}};\nexport default allComponents;`;
    //写入组件列表
    fs.writeFile(`./src/addComponents.js`,file,function(err){
        if(err){
            return console.log('写入组件列表失败', err);
        }else{
            console.log('写入组件列表成功');
            console.log('结束创建文件');
            console.log('>>>>>>>>>>>>>');
        }
    });
}
addComponentsList();
function dealIndexStyle(){
    let file='';
    configData.data.forEach((item) => {
        //遍历所有组件引入每个组件样式 例如：@import "button";
        let name=item.componentsName; //首字母大写
        file+=`@import "${name}";\n`;
    });
    fs.writeFile(`./src/styles/index.scss`,file,function(err){
        if(err){
            return console.log('主样式文件引入scss失败', err);
        }else{
            console.log('主样式文件引入scss成功');
        }
    });
}
dealIndexStyle();
