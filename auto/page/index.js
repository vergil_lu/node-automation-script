exports.indexPage = function (componentsName) {
  return `
import ${componentsName.charAt(0).toUpperCase() + componentsName.slice(1)} from './${componentsName}.vue';
export default ${componentsName.charAt(0).toUpperCase() + componentsName.slice(1)};
  `;
  
};

exports.vuePage = function (componentsName) {
  return `
<template>
    <div class="${componentsName}">

    </div>
</template>
<script>
export default {
  name: '${componentsName}',
  data(){
    return{}
  }
}
</script>
  `;
};

exports.stylePage = function () {
  return `
@charset "UTF-8";
@import "common/var";
@import "mixins/mixins";
  `;
};