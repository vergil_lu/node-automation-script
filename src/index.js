/*
 * @Description: 入口文件
 * @Author: Vergil
 * @Date: 2021-07-14 10:35:20
 * @LastEditTime: 2021-08-31 14:35:12
 * @LastEditors: Vergil
 */
import allComponents from './addComponents';
/**
 * @description: install的函数其实就是Vue插件的一种写法，便于我们在实际项目中引入的时候可以使用 Vue.use 的方式来自动安装我们的整个组件库
 * @param {Vue:注册一个个的组件,options:注册组件的时候传入一些初始化参数}
 * @return {*}
 */
const install = function (Vue) {

  Object.keys(allComponents).forEach(key => {
    Vue.component(key, allComponents[key]);
  });
};

export default install;
